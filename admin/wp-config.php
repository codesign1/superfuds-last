<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'superfuds');

/** MySQL database username */
define('DB_USER', 'sf');

/** MySQL database password */
define('DB_PASSWORD', 'Superfuds1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u.d<k](Tu[]H~v~A=~DWLK%$% JQTYuGl6Pl!N4XkBThM_h/{JhheXVI 4C^;msO');
define('SECURE_AUTH_KEY',  'MKPo8#mia[FH3O,~Zw%`fWYT02YT$[7Hu= atTyn&]&r0I~-c,v{j7D5;v$NHJq!');
define('LOGGED_IN_KEY',    'a%D66X5/e9xX4Fe@7p#N:ZgOH-xAZhhOgJF-l<O>_dGN0&ID05y5zuNdzXGA+GC1');
define('NONCE_KEY',        '.=Z0QT]#~+huTk~#-yF-uO5Dl%+h]`A%5PxZx+0TJIPTc+-*f^u@NRPm46E>v9W>');
define('AUTH_SALT',        'Q,S-LZ3s!!^$ete{P+cjN<*K*NUj>ce@2,o1H-)WP)}vWK![Qnb@{d%n2-*6D0M_');
define('SECURE_AUTH_SALT', 'C9 Klku1I3?+&-da{OQmnb~T1*PoYmS3f!SO<EKaq0fwv%bioLt#+6=t8-O%Rr2d');
define('LOGGED_IN_SALT',   'LCnJ}k,3(lD6X5/ifES3/!UeE>oY0`XAZtqP@laG<:@m}4?G~,)(,5<wllyy|j[!');
define('NONCE_SALT',       'L%>}|B*X>?.b4LA&UDLMhP5*`nj>WL82<8!msZaQMf7pB~@*EA.r[*$!HMsdD6?Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
