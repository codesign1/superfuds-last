<?php

namespace Template\App;

use Velocity\Core\Router;

class RouterConfig {
	public static function execute() {
		/**
		 * These two routes point to the same resource, i.e., controller when accessed
		 * using a GET request
		 */
		Router::get('/', 'home');
		Router::get('/home', 'home');
		Router::get('/blog', 'blog');
		Router::get('/products', 'products');
		Router::get('/search-results', 'search');
		Router::post('/search-results', 'search#get_search');
		Router::get('/continue-register', 'cont');
		Router::post('/continue-register', 'cont');
		Router::get('/thanks', 'thanks');
		Router::post('/thanks', 'thanks');
		Router::get('/privacy', 'privacy');
		Router::get('/leads', 'leads');


		/**
		 * This is a route with basic parameters. It uses PHP regular expressions with named
		 * capture groups. The capture group name is the name of the parameter sent to the
		 * by_id method in the time controller
		 */
		Router::post('/home/(?<lead>[0-9a-zA-Z-]+)', 'home#lead');
		Router::post('/leads', 'leads#login_try');
		Router::get('/leads/(?<lead>[0-9a-zA-Z-]+)', 'leads#show_leads');
		Router::get('/idioma/(?<lang>[0-9a-zA-Z-]+)', 'idioma#change_lang');
		Router::post('/home/(?<lead>[0-9a-zA-Z-]+)/(?<value>[0-9a-zA-Z-]+)', 'home#finalize');
		Router::get('/home/(?<lead>[0-9a-zA-Z-]+)', 'home#intro');
		Router::get('/blog/(?<page>[0-9a-zA-Z-]+)', 'blog#get_posts');
		Router::get('/blog/(?<page>[0-9a-zA-Z-]+)/(?<url>[0-9a-zA-Z-]+)', 'blog#get_blog_post');
		Router::get('/products/(?<cat>[0-9a-zA-Z-]+)', 'products#get_products_cat');

		/**
		 * When using multiple parameters, they are sent in order of appearance to
		 * the corresponding controller method
		 *
		 * Parameters can be defined to be of a certain type, like text or numbers only
		 */
		Router::get('/home/(?<name>[A-z]+)/hour/(?<hour>[0-9]+)/minute/(?<minute>[0-9]+)', 'home#multiple_params');
	}
}
