<?php

namespace template\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class BlogCtrl extends Controller {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $lang,
		    $accion,
		    $posts,
		    $post,
		    $page,
		    $num_pages,
		    $recent_posts;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->lang = Cookie::get('lang');
		$this->accion = 'todos';
		$this->get_posts(1);
		$this->page = 1;
		$this->get_recent_posts();
	}

	public function get_posts($page){

		$this->posts = Array();

		if($this->lang=='es') {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'");
		} else {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles'");
		}
		
		$this->page = $page;
		$num_posts = 3;

		$this->num_pages = ceil(count($posts) / $num_posts);
	
		if(count($posts) > ($page * $num_posts)) {
			$last_post = $page * $num_posts;
			$first_post = $last_post - $num_posts;
		} else {
			$last_post = count($posts);
			$first_post = $last_post - (count($posts) - $num_posts);
		}

		
		$next_page = $page + 1;
		$prev_page = $page - 1;

		for ($i=$first_post; $i < $last_post; $i++) { 
			$id = $posts[$i]->ID;
			$pos = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$title = $posts[$i]->post_title;
			$date = $posts[$i]->post_date;
			$content = $posts[$i]->post_content;
			$img = '';
			$url = '';
			$sumario = '';
			foreach ($pos as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'url') {
					$url = $pro->meta_value;
				} elseif ($pro->meta_key == 'sumario') {
					$sumario = $pro->meta_value;
				}
			}
			if($img != '' && $url != '' && $sumario != '') {
				$this->posts[] = array(
					'title' => $title,
					'date' => $date,
					'img' => $img,
					'url' => $url,
					'sumario' => $sumario,
					'content' => $content
				);
			}
		}
	}

	public function get_all_posts(){
		if($this->lang=='es') {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'");
		} else {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles'");
		}

		foreach ($posts as $key) {
			$id = $key->ID;
			$pos = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$title = $key->post_title;
			$date = $key->post_date;
			$content = $key->post_content;
			$img = '';
			$url = '';
			$sumario = '';
			foreach ($pos as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'url') {
					$url = $pro->meta_value;
				} elseif ($pro->meta_key == 'sumario') {
					$sumario = $pro->meta_value;
				}
			}
			if($img != '' && $url != '' && $sumario != '') {
				$this->posts[] = array(
					'title' => $title,
					'date' => $date,
					'img' => $img,
					'url' => $url,
					'sumario' => $sumario,
					'content' => $content
				);
			}
		}
	}

	public function get_recent_posts(){
		if($this->lang=='es') {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'ORDER BY ID DESC LIMIT 2");
		} else {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles' ORDER BY ID DESC LIMIT 2");
		}
		foreach ($posts as $key) {
			$id = $key->ID;
			$pos = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$title = $key->post_title;
			$date = $key->post_date;
			$content = $key->post_content;
			$img = '';
			$url = '';
			$sumario = '';
			foreach ($pos as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'url') {
					$url = $pro->meta_value;
				} elseif ($pro->meta_key == 'sumario') {
					$sumario = $pro->meta_value;
				}
			}
			if($img != '' && $url != '' && $sumario != '') {
				$this->recent_posts[] = array(
					'title' => $title,
					'date' => $date,
					'img' => $img,
					'url' => $url,
					'sumario' => $sumario,
					'content' => $content
				);
			}
		}
	}

	public function get_blog_post($page, $url){
		$this->get_all_posts();
		$this->accion = 'post';
		foreach ($this->posts as $key) {
			if($key['url'] == $url) {
				$this->post = $key;
			}
		}
	}

}
