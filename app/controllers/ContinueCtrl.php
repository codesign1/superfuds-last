<?php

namespace template\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class ContinueCtrl extends Controller {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $lang,
		    $recent_posts;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->lang = Cookie::get('lang');
		$this->get_recent_posts();
	}

}
