<?php

namespace template\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Timedate;
use Velocity\Helpers\Redirect;
use Velocity\Authentication\Cookie;
use Velocity\Authentication\Input;
use Velocity\Authentication\Validate;
use Velocity\Email\PHPMailer;

class HomeCtrl extends Controller {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $lang,
		    $errors,
		    $status,
		    $email,
		    $headline,
		    $subheadline,
		    $suppliers,
		    $businesses,
		    $recent_posts;

	public function init() {

		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->lang = Cookie::get('lang');
		$this->accion = '';
		$this->headline = $this->cms->query("SELECT option_value FROM wp_options WHERE option_name = 'blogname'")[0]->option_value;
		$this->subheadline = $this->cms->query("SELECT option_value FROM wp_options WHERE option_name = 'blogdescription'")[0]->option_value;
		if($this->lang=='en') {
			$this->suppliers = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'suppliers' ORDER BY ID DESC");
			$this->businesses = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'businesses' ORDER BY ID DESC");
		} else {
			$this->suppliers = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'proveedores' ORDER BY ID DESC");
			$this->businesses = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'negocios' ORDER BY ID DESC");
		}
		$this->get_recent_posts();
	}

	public function lead($url){

		$this->accion = 'continuar';

		if(Input::exists()) {
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'company' => array(
					'required' => true
				),
				'name' => array(
					'required' => true
				),
				'email' => array(
					'required' => true,
					'unique' => 'v_leads'
				),
				'phone' => array(
					'required' => true,
					'unique' => 'v_leads'
				),
				'type' => array(
					'required' => true
				)
			));
			if($validation->passed()) {
				
				$mail = new PHPMailer;
				$mail->setFrom(Input::get('email'), Input::get('nombre'));
				$mail->addReplyTo(Input::get('email'), Input::get('nombre'));
				$mail->addAddress('jpc@estudiocodesign.com', 'Juan Pablo Casabianca');
				$mail->isHTML(true);
				$mail->Subject = 'Lead Notification - Website Superfuds';
				$body = '<h1>Lead Notification: </h1>';
				$body .= '<p>Nombre: ' . Input::get('name') . '</p><br>';
				$body .= '<p>Compañía: ' . Input::get('company') . '</p><br>';
				$body .= '<p>Email: ' . Input::get('email') . '</p><br>';
				$body .= '<p>Telefono: ' . Input::get('phone') . '</p><br>';
				$body .= '<p>Tipo: ' . Input::get('type') . '</p><br>';
				$mail->Body = $body;
				if (!$mail->send()) {
				    $status = $mail->ErrorInfo;
				} else {

					$this->cms->create('v_leads', array(
						'logged' => Timedate::get_mysql_format(),
						'company' => Input::get('company'),
						'name' => Input::get('name'),
						'email' => Input::get('email'),
						'phone' => Input::get('phone'),
						'type' => Input::get('type')
					));

				    $this->status = 'success';
				    $this->email = Input::get('email');
				}

			} else {
				$this->errors = $validation->errors();
			}
		}

	}

	public function finalize($url, $value){
		
		if(Input::exists()) {
			$validate = new Validate();
			$validation = $validate->check($_POST, array(
				'city' => array(
					'required' => true
				),
				'product' => array(
					'required' => true
				),
				'email' => array(
					'required' => true
				)
			));
			if($validation->passed()) {
				
				$mail = new PHPMailer;
				$mail->setFrom(Input::get('email'), Input::get('nombre'));
				$mail->addReplyTo(Input::get('email'), Input::get('nombre'));
				$mail->addAddress('jpc@estudiocodesign.com', 'Juan Pablo Casabianca');
				$mail->isHTML(true);
				$mail->Subject = 'Follow Up Lead Notification - Website Superfuds';
				$body = '<h1>Lead Notification Follow Up: </h1>';
				$body .= '<p>Email: ' . Input::get('email') . '</p><br>';
				$body .= '<p>Ciudad: ' . Input::get('city') . '</p><br>';
				$body .= '<p>Producto: ' . Input::get('product') . '</p><br>';
				$body .= '<p>Comentarios: ' . Input::get('comments') . '</p><br>';
				$mail->Body = $body;
				if (!$mail->send()) {
				    $status = $mail->ErrorInfo;
				} else {

					$email = Input::get('email');
					$lead_id = $this->cms->query("SELECT id FROM v_leads WHERE email = '$email'")[0]->id;

					$this->cms->update('v_leads',$lead_id, array(
						'city' => Input::get('city'),
						'product' => Input::get('product'),
						'comments' => Input::get('comments')
					));

					Redirect::to('/thanks');
				    
				}

			} else {
				$this->errors = $validation->errors();
			}
		}
	}

	public function get_recent_posts(){
		if($this->lang=='es') {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'ORDER BY ID DESC LIMIT 2");
		} else {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles' ORDER BY ID DESC LIMIT 2");
		}
		foreach ($posts as $key) {
			$id = $key->ID;
			$pos = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$title = $key->post_title;
			$date = $key->post_date;
			$content = $key->post_content;
			$img = '';
			$url = '';
			$sumario = '';
			foreach ($pos as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'url') {
					$url = $pro->meta_value;
				} elseif ($pro->meta_key == 'sumario') {
					$sumario = $pro->meta_value;
				}
			}
			if($img != '' && $url != '' && $sumario != '') {
				$this->recent_posts[] = array(
					'title' => $title,
					'date' => $date,
					'img' => $img,
					'url' => $url,
					'sumario' => $sumario,
					'content' => $content
				);
			}
		}
	}

}
