<?php

namespace template\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Helpers\Redirect;
use Velocity\Authentication\Cookie;

class IdiomaCtrl extends Controller {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $lang;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->lang = Cookie::get('lang');
	}

	public function change_lang($lang){
		setcookie('lang', $lang, 0, '/'); // set the default cookie
		Redirect::to($_SERVER["HTTP_REFERER"]);
	}

}
