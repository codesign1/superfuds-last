<?php

namespace template\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;

class ProductsCtrl extends Controller {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $lang,
		    $accion,
		    $products,
		    $categoria,
		    $recent_posts;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->lang = Cookie::get('lang');
		$this->accion = 'todos';
		$this->products = array();
		$this->get_recent_posts();
	}

	public function get_products_cat($categ){

		$this->categoria = str_replace('-', ' ', $categ);

		$this->accion = 'post';
		$products = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'producto'");
		foreach ($products as $key) {
			$id = $key->ID;
			$prod = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$name = $key->post_title;
			$img = '';
			$cat = '';
			$sub_cats = '';
			foreach ($prod as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'categoria') {
					$cat = $pro->meta_value;
				} elseif ($pro->meta_key == 'subcategoria') {
					$sub_cats = $pro->meta_value;
				}
			}
			if($img != '' && $cat != '' && $sub_cats != '' && $categ == $cat) {

				$this->products[] = array(
					'name' => $name,
					'img' => $img,
					'cat' => $cat,
					'sub_cats' => $sub_cats
				);
			}
		}
	}

	public function get_recent_posts(){
		if($this->lang=='es') {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'ORDER BY ID DESC LIMIT 2");
		} else {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles' ORDER BY ID DESC LIMIT 2");
		}
		foreach ($posts as $key) {
			$id = $key->ID;
			$pos = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$title = $key->post_title;
			$date = $key->post_date;
			$content = $key->post_content;
			$img = '';
			$url = '';
			$sumario = '';
			foreach ($pos as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'url') {
					$url = $pro->meta_value;
				} elseif ($pro->meta_key == 'sumario') {
					$sumario = $pro->meta_value;
				}
			}
			if($img != '' && $url != '' && $sumario != '') {
				$this->recent_posts[] = array(
					'title' => $title,
					'date' => $date,
					'img' => $img,
					'url' => $url,
					'sumario' => $sumario,
					'content' => $content
				);
			}
		}
	}

}
