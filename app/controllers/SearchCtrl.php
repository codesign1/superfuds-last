<?php

namespace template\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;
use Velocity\Authentication\Cookie;
use Velocity\Authentication\Input;

class SearchCtrl extends Controller {

	public  $main_slider,	
			$productos,
		    $time,
		    $cities,
		    $is_colombia,
		    $lang,
		    $query,
		    $recent_posts;

	public function init() {
		$this->time = date('H:i');
		$this->cities = array('Bogota', 'Medellin', 'Cali');
		$this->lang = Cookie::get('lang');
		$this->get_recent_posts();
	}

	public function get_search(){
		$this->query = Input::get('s');
	}

	public function get_recent_posts(){
		if($this->lang=='es') {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog'ORDER BY ID DESC LIMIT 2");
		} else {
			$posts = $this->cms->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'blog_ingles' ORDER BY ID DESC LIMIT 2");
		}
		foreach ($posts as $key) {
			$id = $key->ID;
			$pos = $this->cms->query("SELECT * FROM wp_postmeta WHERE post_id = $id");
			$title = $key->post_title;
			$date = $key->post_date;
			$content = $key->post_content;
			$img = '';
			$url = '';
			$sumario = '';
			foreach ($pos as $pro) {
				if($pro->meta_key == 'img') {
					$img = $pro->meta_value;
				} elseif ($pro->meta_key == 'url') {
					$url = $pro->meta_value;
				} elseif ($pro->meta_key == 'sumario') {
					$sumario = $pro->meta_value;
				}
			}
			if($img != '' && $url != '' && $sumario != '') {
				$this->recent_posts[] = array(
					'title' => $title,
					'date' => $date,
					'img' => $img,
					'url' => $url,
					'sumario' => $sumario,
					'content' => $content
				);
			}
		}
	}

}
